# webpack stage
FROM node:14-buster as build
WORKDIR /emojivoto-build
COPY . .
RUN make -C emojivoto-web clean package-web

COPY emojivoto-web/target/ /emojivoto-build/emojivoto-web/target/

# runtime image
FROM debian:buster-20200514-slim
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        curl \
        dnsutils \
        iptables \
        jq \
        nghttp2 \
    && rm -rf /var/lib/apt/lists/*

ARG svc_name
COPY --from=build /emojivoto-build/$svc_name/target/ /usr/local/bin/

# ARG variables are not available for ENTRYPOINT
ENV SVC_NAME $svc_name
WORKDIR /usr/local/bin
ENTRYPOINT $SVC_NAME
