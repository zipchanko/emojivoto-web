.PHONY: web dependency

web:
	$(MAKE) -C emojivoto-web

dependency:
	go mod download
	go get -v -d ./...
